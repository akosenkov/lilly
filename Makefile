CXX = icpc
FLAGS = -O3 -std=c++11
CASES = first.case

.DEFAULT: all
.PHONY: all clean
all: $(CASES)

%.case: %.cpp
	$(CXX) -o $@ $< $(FLAGS)

clean:
	-rm -f $(CASES)
