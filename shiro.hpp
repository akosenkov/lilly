#define MLP_DEPTH 3
#define LIFE_COST 0.01f
#define MAX_CONSUMPTION 10.f

namespace lilly { namespace shiro {

    template<int Bit>
    int getBit(int n){
        return (n & (1 << Bit)) >> Bit;
    }

    struct Neuron {
        using value_type = float;
        using array_type = std::vector<value_type>;

        Neuron(int siblings)
        : input_w(siblings, 0), output_w(siblings, 0),
          income(siblings, 0), outcome(siblings, 0),
          value(0)
        {
            for(auto& w : input_w)  w = ((float)rand() / RAND_MAX);
            for(auto& w : output_w) w = ((float)rand() / RAND_MAX);
            value = ((float)rand() / RAND_MAX);
        }

        float feed(int n){
            return value * output_w[n];
        }

        array_type input_w;
        array_type output_w;
        array_type income;
        array_type outcome;
        value_type value;
    };

    template<class Nr, int M>
    struct Grid : STEntity {
        using value_type = Nr;
        using array_type = std::vector<value_type>;
        using strategy_type = strategy::mlp::perception<MLP_DEPTH,M*4+4>;

        Grid() : container_(M, value_type(M)), dead_(false) {
            strategy_.randomise();
            normalize();
        }

        void die(){
            dead_ = true;
        }

        float is_excited(int N){
            float val = 1/(1+exp(-container_[N].value));
            return val > 0.9;
        }

        virtual void dump(){
            int n = 0;
            for(auto& neuron : container_){
                std::cout << n++ << ": ";
                for(int i = 0; i < M; i++)
                    std::cout << neuron.input_w[i] << " ";
                std::cout << "\n";
            }
            std::cout << "\n";

            std::cout << "Values: ";
            for(auto& neuron : container_)
                std::cout << neuron.value << " ";
            std::cout << "\n";
        }

        virtual void feed(int N, float& amount){
            float value = std::min(MAX_CONSUMPTION, amount);
            container_[N].value += value;
            amount -= value;
        }

        virtual void tick() override {
            if(dead_) return;

            // communicating with mates
            for(int n = 0; n < M; n++){
                auto& neuron = container_[n];
                for(int i = 0; i < M; i++){
                    neuron.income[i] = neuron.input_w[i] * container_[i].feed(n);
                    neuron.outcome[i] = neuron.feed(i) * container_[i].input_w[n];
                }
            }
            for(auto& neuron : container_){
                for(int i = 0; i < M; i++){
                    neuron.value += neuron.income[i] - neuron.outcome[i];
                }
            }

            // spending energy
            for(auto& neuron : container_){
                neuron.value -= LIFE_COST;
                if(neuron.value < 0) die();
            }

            // adapting according to strategy
            double input[M*4+4];
            int nn = 0;
            for(auto& n : container_){
                for(int i = 0; i < M; i++){
                    input[4*i]   = n.income[i];
                    input[4*i+1] = n.outcome[i];
                    input[4*i+2] = n.input_w[i];
                    input[4*i+3] = n.output_w[i];
                }
                input[M*4]   = getBit<0>(nn);
                input[M*4+1] = getBit<1>(nn);
                input[M*4+2] = getBit<2>(nn);
                input[M*4+3] = getBit<3>(nn);

                nn++;
                strategy_ << input;
                auto advice = strategy_.output();
                for(int i = 0; i < M; i++){
                    n.input_w[i] += advice[4*i+2] - 0.5;
                    n.output_w[i] += advice[4*i+3] - 0.5;

                    if(n.value > 100){
                        n.output_w[i] += 0.1f;
                        n.input_w[i] -= 0.1f;
                    }else{
                        n.input_w[i] += 0.1f;
                    }
                }
            }

            normalize();
        }

        void normalize(){
            for(auto& n : container_){
                float total_output = 0;
                for(int i = 0; i < M; i++){
                    n.output_w[i] = std::max(0.f, n.output_w[i]); // positive
                    n.input_w[i] = std::min(1.f, std::max(0.f, n.input_w[i])); // positive <= 1
                    total_output += n.output_w[i];
                }
                if(total_output > 1){
                    for(int i = 0; i < M; i++){ n.output_w[i] /= total_output; }
                }
            }
        }

        array_type container_;
        strategy_type strategy_;
        bool dead_;
    };

} }
