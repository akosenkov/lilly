namespace lilly { namespace sandbox {

    struct Box1 {
        template<class Entity>
        void embed(Entity& e){
            being = &e;
        }

        void evolve(){
            for(int i = 0; i < 5; i++)
            being->tick();
        }

        STEntity* being;
    };

} }
