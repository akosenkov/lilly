struct Food {
    Food(float amount) : food_(amount) {}
    float& get(){
        if(food_ <= 0) std::cout << "FOOD DEPLETED\n";
        return food_;
    }
    void refill(float amount){
        food_ += amount;
    }
    float food_;
};
