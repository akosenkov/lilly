#define activation_function(x) 1/(1+exp(-x)) // sigmoid

namespace lilly { namespace strategy { namespace mlp {

    template<int L>
    layer<L>::layer(){
        neurons = (neuron_type*)std::calloc(L, sizeof(neuron_type));
        for(int i = 0; i < L; i++){
            neurons[i].weights = (double*)std::malloc(L*sizeof(double));
            neurons[i].delta = (double*)std::malloc(L*sizeof(double));
        }
    }

    template<int L>
    layer<L>::~layer(){
        for(int i = 0; i < L; i++){
            std::free(neurons[i].weights);
            std::free(neurons[i].delta);
        }
        std::free(neurons);
    }

    template<int L>
    neuron& layer<L>::operator[](int k){
        return neurons[k];
    }

    template<int L>
    neuron* layer<L>::begin(){
        return neurons;
    }

    template<int L>
    neuron* layer<L>::end(){
        return neurons+L;
    }

    template<int Depth, int Length>
    perception<Depth,Length>::perception() : layers(Depth){
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::operator << (std::pair<double*,double*> tpair){
        (*this) << tpair.first;
        backpropagate(tpair.second);
        adjust_weights();
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::operator << (double* signal){
        for(int i = 0; i < layer_type::length; i++) layers[0][i].value = signal[i];
        propagate_signal();
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::propagate_signal(){
        for(int l = 0; l < layers.size()-1; l++){
            layer_type& front = layers[l];

            std::for_each(layers[l+1].begin(), layers[l+1].end(), [&front](neuron& n){
                n.value = 0;
                for(int i = 0; i < layer_type::length; i++) n.value += n.weights[i] * front[i].value;
                n.value = activation_function(n.value);
            });
        }
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::backpropagate(double* reference){
        // finding output layer error gradient
        average_error = 0.0;
        for(int i = 0; i < layer_type::length; i++){
            double value = layers.back()[i].value;
            double d = reference[i] - value;
            layers.back()[i].error = value * (1.0 - value) * d;
            average_error += fabs(d);
        }
        average_error /= (double)layer_type::length;

        // finding inner layers error gradient
        for(int l = (layers.size()-2); l >= 0; l--)
        for(int j = 0; j < layer_type::length; j++){
            double E = 0.0;
            std::for_each(layers[l+1].begin(), layers[l+1].end(), [j,&E](neuron& n){ E += n.weights[j] * n.error; });
            layers[l][j].error = layers[l][j].value * (1.0 - layers[l][j].value) * E;
        }
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::adjust_weights(){
        for(int l = 1; l < layers.size(); l++)
        for(int j = 0; j < layer_type::length; j++)
        for(int k = 0; k < layer_type::length; k++){
            layers[l][j].delta[k]    = RATE * layers[l-1][k].value * layers[l][j].error + MOMENT * layers[l][j].delta[k];
            layers[l][j].weights[k] += layers[l][j].delta[k];
        }
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::randomise(){
        for(int l = 0; l < layers.size(); l++)
            for(int i = 0; i < layer_type::length; i++)
                for(int k = 0; k < layer_type::length; k++)
                layers[l][i].weights[k] = ((double)rand() / RAND_MAX) - 0.5; // from -0.5 to 0.5
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::print(){
        for(int i = 0; i < layer_type::length; i++) printf("%.2f ", layers.back()[i].value);
        printf("; err: %.2f\n", average_error);
    }

    template<int Depth, int Length>
    double* perception<Depth,Length>::output(){
        for(int i = 0; i < layer_type::length; i++) output_dump[i] = layers.back()[i].value;
        return output_dump;
    }

    template<int Depth, int Length>
    void perception<Depth,Length>::dump(){
        for(int l = 1; l < layers.size(); l++){
            printf("\nLayer %d:", l);
            for(int j = 0; j < layer_type::length; j++){
                printf("\nNeuron %d weights: ", j);
                for(int k = 0; k < layer_type::length; k++){
                    printf("%.2f ", layers[l][j].weights[k]);
                }
            }
        }
        printf("\n");
    }

} } }
