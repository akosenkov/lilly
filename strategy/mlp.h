#define MOMENT 0 //.9
#define RATE 0.025

namespace lilly { namespace strategy { namespace mlp {

    struct neuron {
        double  value;
        double  error;
        double* weights;
        double* delta;
    };

    template<int L>
    class layer {
    public:
        typedef neuron neuron_type;
        static constexpr int length = L;

        layer();
       ~layer();
        neuron& operator[](int k);
        neuron* begin();
        neuron* end();
    private:
        neuron_type* neurons;
    };

    template<int Depth, int Length>
    class perception {
    public:
        typedef layer<Length> layer_type;

        perception();
        void propagate_signal();
        void backpropagate(double* reference);
        void adjust_weights();
        void randomise();
        void operator << (double* signal);
        void operator << (std::pair<double*,double*> tpair);
        void print();
        void dump();
        double* output();

        double output_dump[Length];
        double average_error;
    private:
        std::vector<layer_type> layers;
    };

} } }
