#include "lilly.hpp"
#define L 9
#define TIME_LIMIT 100000
#define FOOD_SUPPLY TIME_LIMIT*100

template<int Spot, typename Being>
bool test_spot(Being& b){
    Food food(FOOD_SUPPLY); int time = 0;
    do {

        b.tick();
        if(b.is_excited(Spot+1))
            b.feed(Spot, food.get());

    } while(!b.dead_ && ++time < TIME_LIMIT);
    return time == TIME_LIMIT;
}

int main(){
    using namespace lilly;
    std::srand(std::time(0));

    while(true){
        shiro::Grid<shiro::Neuron, L> grid;
        if(!test_spot<3>(grid)) continue;
        else{
            grid.dump();
            if(test_spot<5>(grid)) std::cout << "Passed second test as well\n\n";
            if(test_spot<7>(grid)) std::cout << "Passed third test as well\n\n";
        }
    }

    return 0;
}

